/**
 * 
 */
package test;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author ogawa.tsuyoshi
 *
 */
public class FizzBuzzTest {


	/**
	 * {@link test.FizzBuzz#checkFizzBuzz(int)} のためのテスト・メソッド。
	 */
	@Test
	public void testCheckFizzBuzz() {
		assertEquals("Fizz", FizzBuzz.checkFizzBuzz(9));
		assertEquals("Buzz", FizzBuzz.checkFizzBuzz(20));
		assertEquals("FizzBuzz", FizzBuzz.checkFizzBuzz(45));
		assertEquals("44", FizzBuzz.checkFizzBuzz(44));
		assertEquals("46", FizzBuzz.checkFizzBuzz(46));
	}


}
